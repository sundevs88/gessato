<?php

/**
 * Block for metatags
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Block_Page_Html_Head_Metatags extends Creatuity_RichPins_Block_Abstract {

    protected function _construct() {
        if ($this->_isEnabled()) {
            $this->setTemplate('creatuity/richpins/page/html/head/metatags.phtml');
        }
    }

    protected function _isEnabled() {
        return parent::_isEnabled() && $this->_isMetatagsMethodSelected();
    }

    protected function _isMetatagsMethodSelected() {
        return $this->_getConfigHelper()->getSelectedMethod() == 'tags';
    }

    public function getAttributes() {
        $model = Mage::getModel('richpins/product_attributes_provider');
        $params = new Varien_Object(array('product' => Mage::registry('product')));
        return $model->getValues('metatag', 'simple_product', $params);
    }

}