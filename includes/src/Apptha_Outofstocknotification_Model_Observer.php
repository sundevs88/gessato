<?php
class Apptha_Outofstocknotification_Model_Observer extends Mage_Core_Model_Abstract 
{
	 private $stockNotifiTable;
	 private $read;
	 private $bcc;
	 private $productName;
	 private $productUrl;
	 private $prodcutImg;
	 private $siteLink;
	 private $productDescr;
	 private $storeName;
	
	const XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION = 'Outofstocknotification/outofstock_email/outofstock_credit_template';
	                                                 
    public function _construct()
    {
    	
        parent::_construct();
        $this->_init('outofstocknotification/outofstocknotification');
        $isArray      = count($_GET);
        $resource     = Mage::getSingleton('core/resource');
        $resource     = Mage::getSingleton('core/resource');
        $this->read   = $resource->getConnection('write');
        $tPrefix      = (string) Mage::getConfig()->getTablePrefix();
        $this->stockNotifiTable = $tPrefix . 'outofstocknotification';
        $this->siteLink         = Mage::getBaseUrl();
    }
    public function newproductadding( $observer)
    {   
    	
    }
    public function sendMailToNotifiedCustomerOk($observer){
        	
    	
    	$enableOutOfStock  = Mage::getStoreConfig('Outofstocknotification/general/activate_apptha_outofstock_enable'); //backend you given module status in off then dont exe this codings
    	$product           = $observer->getProduct();  //get the current product
    	$isInStock         = $product['stock_data']['is_in_stock'];
        $enableOutOfStock  = intval($enableOutOfStock); //if the product is out of stock val = 0 , that time dont send mails
        $productUrl        = $product->getUrlInStore();
        $stockLevel        = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
        $status            = $product->getStatus(); 
       
        //echo "<pre>";    	print_r($product);echo "</pre>";die;

        if($product->_isObjectNew)
    	{
    	   return 1;
    	}
    	
        if(!$isInStock) //if it is a out of stock product no need to do any task
        { 
        	return 1;
        }
        if(!$enableOutOfStock)  // if out of stock is disable then dont do any task just return
        { 
        	return 1; //if outof stock notifi status is no then dont exe all funs
        }
   	$this->storeName = Mage::getStoreConfig("general/store_information/name");  
    $this->productDescr =  $product->getDescription();
     
    $getProductImageList = json_decode($product->_data['media_gallery']['images']); //get images
        for($i = 0 ; $i < count($getProductImageList) ; $i++ )
		{
			if(!$getProductImageList[$i]->removed)
			{ 
							if(!$getProductImageList[$i]->disabled && $getProductImageList[$i]->position == 1  && !$getProductImageList[$i]->removed){
					$prodcutImageIs = $getProductImageList[$i]->url;
					break;
				} 
				else if(!$getProductImageList[$i]->disabled  &&  !$getProductImageList[$i]->removed){
					$prodcutImageIs = $getProductImageList[$i]->url;
				}
			}
		}
		$this->prodcutImg = $prodcutImageIs;
     if(! strlen($this->prodcutImg) ) {
      	
      	$this->prodcutImg =  Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).DS.'frontend'.DS.'default'.DS.'default'.DS.'outofstocknotification'.DS.'defaultimage.jpg';
       }
        

    if(trim($product->_data['type_id']) == 'grouped' ||  trim($product->_data['type_id']) == 'bundle')
      {
      	$stockLevel = 2; //quantity is not check for grouped
      }
    
    	 if($status && ($stockLevel > 0))
    	 {       
    	 	      $productId   = $product->getId();
        	  	  $mailFunCallOrNot = $this->isProductInNotifiyList($productId);  //find this product in notify list or not
		     	 if($mailFunCallOrNot)
		    	 {
		    	 	$this->_sendNotificationEmail($this->bcc);
		    	 	 $this->updateMailAndStatusOfNotifiy($productId); //changes the status of mailsend_status and status
		    	 }
	  	 }
    	 else{
    	 	
    	   		return false;  //product is out of stock
    	 }
       
     }//function is end hear
     
     private function isProductInNotifiyList($productId)
     {
	     	$this->bcc = array();
	      	$mailStaus = 'NO';
	        $query = "SELECT  DISTINCT email_id , product_name , product_url  FROM  $this->stockNotifiTable WHERE product_id = $productId AND status = 1 AND mailsend_status = '$mailStaus'";
	        $data = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($query);
	        $isArray = count($data);
	        if($isArray)
		    {
			    foreach($data as $key => $productList)
		        {
		               $this->bcc[] = $productList['email_id'];
		        }
		        $this->productUrl  = $productList['product_url'];
		        $this->productName = $productList['product_name'];
		        return 1;
		    }
		    else{
		    	 return 0;	
		    }
        
     }//function end hear

 
    public function _sendNotificationEmail($to, $templateConfigPath = self::XML_PATH_EMAIL_ADMIN_QUOTE_NOTIFICATION )
    {
        if (! $to) return;
         
          $sendTo = array();       
        
        foreach ($to as $recipient)
        {
            if (is_array($recipient))
            {
                $sendTo = $recipient;
            }
            else
            {
                $sendTo = array(
                    'email' => $recipient,
                    'name' => null,
                );
            }
        } 
    	$this->productDescr = substr($this->productDescr , 0 , 420);
		if(strlen($this->productDescr) > 420)
		{
			$this->productDescr .= '...';
		}
		$emailTemplateVariables = array();
		$emailTemplateVariables['productName'] = $this->productName;
		$emailTemplateVariables['productUrl']  = $this->productUrl;
		$emailTemplateVariables['productImg' ] = $this->prodcutImg;
		$emailTemplateVariables['storeName']   = $this->storeName;
		$emailTemplateVariables['siteLink']    = $this->siteLink;
		$emailTemplateVariables['productDesc'] = $this->productDescr;
		
		
        $marchentNotificationMailId  = Mage::getStoreConfig('Outofstocknotification/outofstock_email/outofstock_sender_email_identity');
        $senderMailId                = Mage::getStoreConfig("trans_email/ident_$marchentNotificationMailId/email");
        $senderName                  =  Mage::getStoreConfig("trans_email/ident_$marchentNotificationMailId/name");
	    $templeId                    = (int)Mage::getStoreConfig('Outofstocknotification/outofstock_email/outofstock_credit_template');
	    
        if($templeId)   //if it is user template then this process is continue
        {
        	$emailTemplate  = Mage::getModel('core/email_template')->load($templeId);
        }
        else{   //  we are calling default template 
        		$emailTemplate  = Mage::getModel('core/email_template')
	        	->loadDefault('outofstock_email_template'); 
	     }   
	        	$emailTemplate->setSenderName($senderName);     //mail sender name
	        	$emailTemplate->setSenderEmail($senderMailId);  //mail sender email id
	        	$emailTemplate->setTemplateSubject('Out of stock Notification from '.$this->storeName );
 				$emailTemplate->setDesignConfig(array('area' => 'frontend'));
	        	$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables); //it return the temp body
	        	foreach ($to as $recipient) {
				$emailTemplate->send($recipient,$senderName, $emailTemplateVariables);  //send mail to customer email ids
				}
  }
     
     private function updateMailAndStatusOfNotifiy($productId)  //updated notified mails in DB
     {
     	   $deleteNotify = (int)Mage::getStoreConfig('Outofstocknotification/general/delete_apptha_outofstock_mail');
     	   $deleteNotify = intval($deleteNotify);
     	   
     	   if($deleteNotify)
     	   {
     	      	$query = "DELETE FROM $this->stockNotifiTable   WHERE product_id = $productId ";
     	   }
     	   else{
     	   		$date         = date("M d, Y");
	       		$mailSend     = 'YES';
     	   		$deleteNotify = 1;
     	   		$query = "UPDATE $this->stockNotifiTable SET  mailsend_status = '$mailSend' , status = $deleteNotify , update_time ='$date'   WHERE product_id = $productId ";
     	   }
	     	//IF MAIL IS SEND SET status = 0 AND mailsend_status = 1
	 	   	$data  = Mage::getSingleton('core/resource')->getConnection('core_read')->query($query);
	   
     }
}//class is end hear
