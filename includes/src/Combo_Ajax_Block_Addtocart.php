<?php

class Combo_Ajax_Block_Addtocart extends Mage_Core_Block_Template
{
    
    private $product;
    private $error;
    private $quantity;

    protected function _beforeToHtml()
    {
            parent::_beforeToHtml();

            if (!$this->getTemplate()) {
                    $this->setTemplate('checkout/add_to_cart_ajax.phtml');
            }

            return $this;
    }

    protected function _toHtml() {
        return parent::_toHtml();
    }
    
    public function setProduct($product, $quantity) {
        $this->product = $product;
        $this->quantity = $quantity;
        return $this;
    }

    public function setError($error) {
        $this->error = $error;
        return $this;
    }
    
    public function getProduct() {
        return $this->product;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function getError() {
        return $this->error;
    }

    public function formatPrice($p) {
        return Mage::helper('core')->currency($p, true, false);
    }
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }

    public function getCartItemsCount() {
        return $this->_getCart()->getItemsCount();
    }

    public function getCartSubtotal() {
        return $this->_getCart()->getQuote()->getSubtotal();
    }

    public function hasError() {
        return $this->error != null && !empty($this->error);
    }
}
