<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Model_Product_Attributes_Values {

    const VALUE_INSTOCK = 'in stock';
    const VALUE_OUTOFSTOCK = 'out of stock';

    /**
     * Get product name
     * 
     * @return string 
     */
    public function getProductName(Varien_Object $params) {
        return $params->getProduct()->getName();
    }

    /**
     * Get product price amount
     * 
     * @return string 
     */
    public function getProductPriceAmount(Varien_Object $params) {
        return $params->getProduct()->getFinalPrice();
    }

    /**
     * Get product price amount for configurable product's child
     * 
     * @param Varien_Object $params
     */
    public function getConfigurableProductPriceAmount(Varien_Object $params) {
        $product = $params->getProduct();
        $price = $params->getParentPrice();
        
        foreach ($params->getAttributesArray() as $attributes) {
            foreach ($attributes['options'] as $option) {
                if (in_array($product->getId(), $option['products'])) {
                    $price += $option['price'];
                }
            }
        }
        return $price;
    }
    
    /**
     * Get product price currency
     * 
     * @return string 
     */
    public function getProductPriceCurrency() {
        return Mage::app()->getStore()->getCurrentCurrency()->getCode();
    }

    /**
     * Get site name
     * 
     * @return string 
     */
    public function getSiteName() {
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        return Mage::getModel('core/website')->load($websiteId)->getName();
    }

    /**
     * Get URL of the product page
     * 
     * @return string
     */
    public function getProductUrl(Varien_Object $params) {
        return Mage::getModel('core/url')->getUrl($params->getProduct()->getUrlPath());
    }

    /**
     * Get product description
     * 
     * @return string 
     */
    public function getProductDescription(Varien_Object $params) {
        return $params->getProduct()->getShortDescription();
    }

    /**
     * Get product stock availability
     * 
     * @return string 
     */
    public function getStockAvailability(Varien_Object $params) {
        $product = $params->getProduct();
        return $product->isInStock()
                && $product->getIsInStock() ? self::VALUE_INSTOCK : self::VALUE_OUTOFSTOCK;
    }

}
