<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
class Stephanetravostino_Commercebug_Model_System_Config_Source_Log_Format_Classes
{
    public function toOptionArray()
    {
        return array(
        'Stephanetravostino_Commercebug_Helper_Formatlog_Raw'=>'Raw JSON Notation',
        'Stephanetravostino_Commercebug_Helper_Formatlog_Raw'=>'All Information, Simple Text',
        'custom'=>'Custom Class'
        );
    }
}