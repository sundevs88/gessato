<?php

class Combo_Ajax_WishlistController extends Mage_Core_Controller_Front_Action
{
    private function response($success, $message, $href) {
        $response = array(
            "success" => $success,
            "message" => $message,
            "href" => $href,
        );
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function addAction() {
        $wishlist = Mage::helper('wishlist')->getWishlist();

        $productId = (int) $this->getRequest()->getParam('product');
        if (!$productId) {
            return $this->response(false, "Product not specified.", null);
        }

        $product = Mage::getModel('catalog/product')->load($productId);
        if (!$product->getId() || !$product->isVisibleInCatalog()) {
            return $this->response(false, "Cannot specify product.", null);
        }

        $redirect = Mage::helper('wishlist')->getAddUrl($product);

        try {
            $wishlist->addNewItem($product);
        } catch (Exception $e) {
            Mage::log($e->getMessage());
            return $this->response(false, "An error occurred while adding the product to your wishlist.", $redirect);
        }

        $href = Mage::helper('combo/wishlist')->getRemoveProductUrl($product);
        $this->response(true, "Product added to your wishlist", $href);
    }

    public function removeAction() {
        $wishlist = Mage::helper('wishlist')->getWishlist();

        $itemId = (int) $this->getRequest()->getParam('item');
        if (!$itemId) {
            return $this->response(false, "Wishlist item not found.", null);
        }

        $item = Mage::getModel('wishlist/item')->load($itemId);
        if (!$item->getId()) {
            return $this->response(false, "Cannot specify wishlist item.", null);
        }

        $redirect = Mage::helper('wishlist')->getRemoveUrl($item);

        try {
            $item->delete();
        } catch (Exception $e) {
            Mage::log($e->getMessage());
            return $this->response(false, "An error occurred while removing the product from your wishlist.", $redirect);
        }

        $href = Mage::helper('wishlist')->getAddUrl($item->getProduct());

        $this->response(true, "Product removed from your wishlist", $href);
    }
}
