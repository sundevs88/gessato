<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Stephanetravostino_Commercebug_Block_Tab_Lookup extends Stephanetravostino_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_lookup.phtml');
    }		
}