<?php

class Apptha_Outofstocknotification_Model_Mysql4_Outofstocknotification_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('outofstocknotification/outofstocknotification');
    }
}