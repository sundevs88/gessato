<?php
/**
 * Pre-Orders
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitpreorder
 * @version      1.2.11
 * @license:     5soxLTftyQhxyplGaD4S3STlnowRzK2YqbvycBpgEF
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
class Aitoc_Aitpreorder_Model_Catalog_Product_View extends Aitoc_Aitpreorder_Model_Abstract {

    protected function _toHtml($html)
    {
        if ($this->getBlock()->getNameInLayout() == 'product.info.addtocart' && $this->getBlock()->getProduct()->getPreorder()) {
            $html = str_replace($this->__('Add to Cart'), $this->__('Pre-Order'), $html);
        }

        return $html;
    }

}