<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Stephanetravostino_Commercebug_Block_Tab_Welcome extends Stephanetravostino_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_welcome.phtml');
    }		
}