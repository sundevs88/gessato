<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
abstract class Creatuity_RichPins_Model_Product_Type_Base extends Creatuity_RichPins_Model_Product_Type_Abstract {

    protected function _getValues() {
        return $this->getProvider()->getValues(
                'oembed', 'simple_product', new Varien_Object(array('product' => $this->getProduct()))
        );
    }

}
