<?php
class Apptha_Outofstocknotification_Block_Outofstocknotification extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getOutofstocknotification()     
     { 
        if (!$this->hasData('outofstocknotification')) {
            $this->setData('outofstocknotification', Mage::registry('outofstocknotification'));
        }
        return $this->getData('outofstocknotification');
        
    }
}