<?php

class Apptha_Outofstocknotification_Model_Mysql4_Outofstocknotification extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the outofstocknotification_id refers to the key field in your database table.
        $this->_init('outofstocknotification/outofstocknotification', 'outofstocknotification_id');
    }
}