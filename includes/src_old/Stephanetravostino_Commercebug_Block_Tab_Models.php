<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/

class Stephanetravostino_Commercebug_Block_Tab_Models extends Stephanetravostino_Commercebug_Block_Html
{
    public function __construct()
    {						
        $this->setTemplate('tabs/ascommercebug_models.phtml');
    }		
}