<?php

class Czettner_Ajax_Block_Product extends Mage_Catalog_Block_Product
{
    
    private $product;

    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime'    => 120,
            'cache_tags'        => array(Mage_Catalog_Model_Product::CACHE_TAG . "_" . $this->getProduct()->getId()),
            'cache_key'         => $this->getProduct()->getId(),
        ));
    }

    protected function _beforeToHtml()
    {
            parent::_beforeToHtml();

            if (!$this->getTemplate()) {
                    $this->setTemplate('ajax/quickview.phtml');
            }

            return $this;
    }

    protected function _toHtml() {
        return parent::_toHtml();
    }
    
    public function setProduct($product) {
        $this->product = $product;
        return $this;
    }
    
    public function getProduct() {
        return $this->product;
    }
}
