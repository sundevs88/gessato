<?php

class Gessato_Helloworld_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function productsAction()
    {
        $products = Mage::getModel('catalog/product')->getCollection();
        echo "count: " . count($products);
    }

    public function categoriesAction()
    {
        $model = Mage::getModel('catalog/category');
        $categories = $model->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('level')
            ->addFieldToFilter('is_active', true)
            ->addFieldToFilter('level', 2);
        $categories->load();
        echo "<h1>" . count($categories) . " categorie</h1>";
        echo "<ul>";
        foreach ($categories as $category) {
            $subcategories = $category->getChildrenCategories();
            echo "<li>livello " . $category->getLevel() . ": " . $category->getName();
            echo "<ul>";
            foreach ($subcategories as $subcategory) {
                echo "<li>" . $subcategory->getName() . "</li>";
            }
            echo "</ul>";
            echo "</li>";
        }
        echo "</ul>";
    }

    public function goodbyeAction()
    {
        echo "Farewell, my dear.";
    }

    public function sendamessageAction()
    {
        $nameSender = $_POST['name'];
        $emailSender = $_POST['email'];
        $message = $_POST['comments'];
        $subject = $_POST['subject'];
        // Set sender information
        $sender = array('name' => $nameSender,
            'email' => $emailSender);
        // Set recepient information
        $recepientEmail = 'info@gessato.com';
        $recepientName = 'GSelect';
        // Get Store ID
        $storeId = Mage::app()->getStore()->getId();
        // Set variables that can be used in email template
        $vars = array('customerName' => $nameSender,
            'customerEmail' => $emailSender, 'customerQuestion' => $message, 'questionSubject' => $subject);
        $translate = Mage::getSingleton('core/translate');
        // Send Transactional Email
        $result = Mage::getModel('core/email_template');
        try {
            $result->sendTransactional('10', $sender, $recepientEmail, $recepientName, $vars, $storeId);
            $translate->setTranslateInline(true);
            echo "<div class=\"thankyou-msg\"><h2>Thank You!</h2><p>We will get back to you as soon as possible.</p><a class=\"close-ajax close-success\" value=\"Close\" onClick=\"window.location.reload()\">close</a></div>";
        } catch (Exception $ex) {
            Mage::getSingleton('core/session')->addError(Mage::helper('helloworld')->__('Unable to send email.'));
        }
    }

    public function paramsAction()
    {
        echo '
<dl>';
        foreach ($this->getRequest()->getParams() as $key => $value) {
            echo '
    <dt>' . $key . '</dt>
    ';
            echo '
    <dd>' . $value . '</dd>
    ';
        }
        echo '
</dl>';
    }
}

?>
