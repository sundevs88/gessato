<?php

class Combo_Ajax_Helper_Wishlist extends Mage_Core_Helper_Abstract
{
    function getRemoveProductUrl($product)
    {
        $wishlist = Mage::helper('wishlist')->getWishlist();
        $item = Mage::getModel("wishlist/item")->loadByProductWishlist(
            $wishlist->getId(),
            $product->getId(),
            Mage::helper('core')->getStoreId()
        );

        return Mage::helper('wishlist')->getRemoveUrl($item);
    }

    // Ritorna true se il prodotto e' in wishlist
    function productInWishlist($product)
    {
        $session = Mage::getSingleton('customer/session');
        $cidData = $session->isLoggedIn();
        $customer_id = $session->getId();

        $count = 0;

        if($customer_id){
            $wishlist = Mage::getModel('wishlist/item')->getCollection();
            $wishlist->getSelect()
                      ->join(array('t2' => 'wishlist'),
                             'main_table.wishlist_id = t2.wishlist_id',
                             array('wishlist_id','customer_id'))
                             ->where('main_table.product_id = '.$product->getId().' AND t2.customer_id='.$customer_id);
            $count = $wishlist->count();
        }

        return $count > 0;
    }
}
