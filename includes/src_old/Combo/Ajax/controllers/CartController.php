<?php
require_once 'Mage/Checkout/controllers/CartController.php';

class Combo_Ajax_CartController extends Mage_Checkout_CartController {
    public function countAction() {
        //$resp = array(
            //"count" => Mage::getSingleton('checkout/cart')->getItemsCount()
        //);

		$layout =  Mage::getSingleton('core/layout');
        $sidebar = $layout
                ->createBlock('checkout/cart_sidebar')
                ->addItemRender('simple', 'checkout/cart_item_renderer', 'checkout/cart/sidebar/default.phtml')
                ->addItemRender('configurable', 'checkout/cart_item_renderer_configurable', 'checkout/cart/sidebar/default.phtml')
                ->addItemRender('grouped', 'checkout/cart_item_renderer_grouped', 'checkout/cart/sidebar/default.phtml')
                ->addItemRender('bundle', 'bundle/checkout_cart_item_renderer', 'checkout/cart/sidebar/default.phtml');
		$sidebar->setTemplate('checkout/cart/sidebar.phtml');

		$resp = array(
			"count" => Mage::getSingleton('checkout/cart')->getSummaryQty(),
            "cartdata" => $sidebar->renderView()
        );


        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($resp)
        );       
    }

    public function addAction() {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        $product = null;
        $error = null;
        $quantity = 1;

        if($params['isAjax'] != 1) {
            return parent::addAction();
        }
        $response = array();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                        array('locale' => Mage::app()->getLocale()->getLocaleCode())
                        );
                $params['qty'] = $filter->filter($params['qty']);
                $quantity = $params['qty'];
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                $error = 'Unable to find Product ID';
            } else {
                $cart->addProduct($product, $params);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }

                $cart->save();

                $this->_getSession()->setCartWasUpdated(true);

                /**
                 * @todo remove wishlist observer processAddToCart
                 */
                Mage::dispatchEvent('checkout_cart_add_product_complete',
                        array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
                        );

                if ($cart->getQuote()->getHasError()){
                    $error = "There's been a problem adding the item to cart.";
                }
            }
        } catch (Mage_Core_Exception $e) {
            $msg = "";
            if ($this->_getSession()->getUseNotice(true)) {
                $msg = $e->getMessage();
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $msg .= $message.'<br/>';
                }
            }
            $error = $msg;
        } catch (Exception $e) {
            $error = "There's been a problem adding the item to cart.";
            Mage::logException($e);
        }


        $block = $this->getLayout()->createBlock('combo/addtocart')
            ->setTemplate('checkout/add_to_cart_ajax.phtml')
            ->setProduct($product, $quantity)
            ->setError($error);
        $this->getResponse()->setBody( $block->renderView() );
    }

}
