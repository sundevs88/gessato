<?php

/**
 * Block for Pinterest button
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Block_Pinterest_Button extends Creatuity_RichPins_Block_Abstract {

    protected function _construct() {
        if ($this->_isEnabled()) {
            $this->setTemplate('creatuity/richpins/pinterest/button.phtml');
        }
    }

    protected function _isEnabled() {
        return parent::_isEnabled() && !$this->_getConfigHelper()->buttonExists();
    }

    /**
     * Get URL of the product page
     * 
     * @return string
     */
    public function getProductUrl() {
        if ($this->getProduct()
                && $this->getProduct()->getId()) {
            return Mage::getModel('core/url')->getUrl($this->getProduct()->getUrlPath());
        }
        return Mage::helper('core/url')->getHomeUrl();
    }

    /**
     * Get product description
     * 
     * @return string 
     */
    public function getProductDescription() {
        if ($this->getProduct()
                && $this->getProduct()->getId()) {
            return $this->getProduct()->getShortDescription();
        }
        return '';
    }

    /**
     * Get URL of product's image
     * 
     * @return string 
     */
    public function getProductMedia() {
        if ($this->getProduct()
                && $this->getProduct()->getId()) {
            return $this->getProduct()->getImageUrl();
        }
        return '';
    }

    /**
     * Get the product
     * 
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct() {
        return $this->getData('product') ? $this->getData('product') : Mage::registry('product');
    }

}
