<?php

/**
 * Product:       Xtento_AdvancedOrderStatus (1.0.7)
 * ID:            9rRzi6pWJU8Iy5dZGp3gVJHZBf9Ga/Rx9BXWbA92gS8=
 * Packaged:      2013-08-28T02:27:50+00:00
 * Last Modified: 2012-06-04T23:39:55+02:00
 * File:          app/code/local/Xtento/AdvancedOrderStatus/Model/Mysql4/Status/Notification/Collection.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_AdvancedOrderStatus_Model_Mysql4_Status_Notification_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('advancedorderstatus/status_notification');
    }
}