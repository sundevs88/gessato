jQuery(document).ready(function($) {
    $(".quick-view").colorbox({transition:"fade", maxWidth:"100%", maxHeight: "100%", scrolling:false, opacity: 0.6, onComplete : function() {
        $(this).colorbox.resize();
    }  });
    $(".callbacks").colorbox({
        onOpen:function(){ alert('onOpen: colorbox is about to open'); },
        onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
        onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
        onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
        onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
    });
    $("div#brand-list").smoothDivScroll({
        autoScrollingMode: "onStart"
    });
});