var Gessato = {
    cartCounter: '#cart-count',
    cartCounter1: '.mobile-cart-count',
    cartBox: '.pull-right #cartboxUpdate',
    cartBox1: 'mobile-cart-top #cartboxUpdate',
    updateCartCount: function(n) {
        jQuery.ajax({
            url: '/combo/cart/count',
            type: 'get',
            dataType: 'json',
            success: function(data) {
                n = parseInt(data.count);
                if (isNaN(n)){
                    n = 0;
                }
                if(n>1){
                    n1 = n+' items'
                }
                else{
                    n1 = n+' item'
                }
                renderData = data.cartdata;
                jQuery(Gessato.cartCounter).text(n).fadeIn();
                jQuery(Gessato.cartCounter1).text(n1).fadeIn();
                jQuery(Gessato.cartBox).html(renderData);
                jQuery(Gessato.cartBox1).html(renderData);
            },
        });
    },
    addToCart: function($cart_form, $spinner) {

        var action = $cart_form.attr('action');

        if (action.indexOf('/checkout/cart/add') == -1) {
            $cart_form.submit();
            return;
        }

        action = action.replace('/checkout/cart/add', '/combo/cart/add');
        var data = $cart_form.serialize() + "&isAjax=1";

        $spinner.spin();

        jQuery.ajax({
            url: action,
            type: 'post',
            data: data,
            dataType: 'html',
            success: function(data) {
                $spinner.spin(false);
                Gessato.updateCartCount();
                jQuery.colorbox({html: data, scrolling:false, opacity:0.6});
            },
            error: function(jqXHR, status, error) {
                console.log(arguments);
                $spinner.spin(false);
            }
        });

    },
};

(function($) {

    $.fn.spin = function(arg) {
        var opts = {
            lines: 11, // The number of lines to draw
            length: 4, // The length of each line
            width: 2, // The line thickness
            radius: 3, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: true, // Whether to use hardware acceleration
            className: 'spinner-inner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
        };
        return this.each(function() {
            var $this = $(this),
                data = $this.data();

            if (data.spinner) {
                data.spinner.stop();
                delete data.spinner;
            }
            if (arg !== false) {
                data.spinner = new Spinner(opts).spin(this);
            }
        });
    }


    /****************************
     * Tabs                     *
     * with class .gessato-tabs *
     ****************************/

    $.fn.gessatoTabs = function() {

        var $active, $content, $links = this.find('ul.tabs a.tab-index');
        $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active.attr('href'));

        // Hide the remaining content
        $links.not($active).each(function () {
            $($(this).attr('href')).hide();
        });

        // Bind the click event handler
        $(this).on('click', 'ul.tabs a:not(.disabled)', function(e){
            // Make the old tab inactive.
            $active.removeClass('active');

            // Make the new tab active.
            $active = $(this);
            $active.addClass('active');

            // Fade out old tab
            $content.fadeOut('fast', function() {

                // Fade in new tab
                $content = $($active.attr('href'));
                $content.fadeIn();

            });


            // Prevent the anchor's default click action
            e.preventDefault();
        });
    };

    $(document).ready(function() {
        $('.gessato-tabs').gessatoTabs();
    });


    /*** Add/remove wishlist AJAX ****/
    $(document).ready(function() {

        $('.ajax-wishlist').on('click', '.ajax-add-wishlist', function(e) {
            var $self = $(this);
            var href = $self.attr('href');

            if (href.indexOf('/wishlist/index/add') == -1) {
                return;
            }
            e.preventDefault();
            if (window.location.protocol == "http:") {
                href=href.replace("https://","http://");
            }
            href = href.replace('/wishlist/index/add', '/combo/wishlist/add');

            jQuery.ajax({
                url: href,
                type: 'get',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        $self.addClass('added-w');
                        $self.removeClass('ajax-add-wishlist').addClass('ajax-remove-wishlist');
                        $self.attr('href', data.href);
                    } else if (data.href != null) {
                        document.location.href = data.href;
                    }
                },
            });

        });

        $('.ajax-wishlist').on('click', '.ajax-remove-wishlist', function(e) {
            var $self = $(this);
            var href = $self.attr('href');

            if (href.indexOf('/wishlist/index/remove') == -1) {
                return;
            }
            e.preventDefault();
            if (window.location.protocol == "http:") {
                href=href.replace("https://","http://");
            }

            href = href.replace('/wishlist/index/remove', '/combo/wishlist/remove');

            jQuery.ajax({
                url: href,
                type: 'get',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        $self.removeClass('added-w');
                        $self.removeClass('ajax-remove-wishlist').addClass('ajax-add-wishlist');
                        $self.attr('href', data.href);
                    } else if (data.href != null) {
                        document.location.href = data.href;
                    }
                },
            });

        });

    });

})(jQuery);

jQuery(document).ready(function($) {
    $("div#category-node-263").smoothDivScroll({
        autoScrollingMode: "onStart",
        scrollableAreaClass: "scrollableArea-263",
        //setupComplete: function () { $("#nav>li .level0").css({"display":"none", "opacity":"1", "height":"378px", "padding":"60px 0"})}
    });
    $("div#category-node-264").smoothDivScroll({
        autoScrollingMode: "onStart",
        scrollableAreaClass: "scrollableArea-264"
    });
    $("div#category-node-265").smoothDivScroll({
        autoScrollingMode: "onStart",
        scrollableAreaClass: "scrollableArea-265"
    });
    $('#contactForm').motionCaptcha();
    $(window).scroll(function () {
                if ($(window).scrollTop() >= 100) {
                    $('.header-container').addClass('sticky');
                    $('.top-link-container').addClass('hidden-sticky');
                    $('.sticky-logo').addClass('show-sticky-logo');
                    $('.header-wrapper div.shad').addClass('shad-fixed');
                } else {
                    $('.header-container').removeClass('sticky');
                    $('.top-link-container').removeClass('hidden-sticky');
                    $('.sticky-logo').removeClass('show-sticky-logo');
                    $('.header-wrapper div.shad').removeClass('shad-fixed');
                }



    });
});
jQuery(document).ready(function(){
    jQuery("#mobile-menu-icon").on("click", function(){
        jQuery(".mobile-menu-content").slideToggle();
    });
    jQuery("#top-link-icon").on("click", function(){
        jQuery(".top-link-content").slideToggle();
    });
    jQuery(".top-link-content a").click(function(){
        jQuery('.top-link-content').hide();
    });
    jQuery('.mobile-menu-content').find('li.parent').append('<strong></strong>');
    jQuery('.mobile-menu-content li.parent strong').on("click", function(){
        if (jQuery(this).attr('class') == 'opened') { jQuery(this).removeClass().parent('li.parent').find('> ul').slideToggle(); }
        else {
            jQuery(this).addClass('opened').parent('li.parent').find('> ul').slideToggle();
        }
    });
    jQuery('.header-box-sticky .mobile-cart-top').hover(function(){
        jQuery(this).find('#cartboxUpdate').stop(true, true).fadeToggle();
    });
    jQuery('.pull-right .cart-top').hover(function(){
        jQuery(this).find('#cartboxUpdate').stop(true, true).fadeToggle();
    });
});

jQuery(document).ready(function () {
    jQuery('a.gallery').colorbox({
        opacity: 0.5, rel: 'gallery', maxWidth: "100%", maxHeight: "100%", speed : 0
    });
    jQuery('#click-view-gallery').click(function(e) {
        jQuery('a.gallery').colorbox({
            opacity: 0.5, rel: 'gallery', maxWidth: "100%", maxHeight: "100%", speed : 0,  open: true
        });
        e.preventDefault();
    });
    jQuery('.product-image-thumbs').bxSlider({
        infiniteLoop: false,
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 67,
        nextText: '',
        prevText: '',
        auto: true
    });
});
jQuery(document).ready(function(){
    //Brand page
    var brandItemWidth = jQuery('#brand-list .item').width();
    jQuery('#brand-list .item > a').css({'height':brandItemWidth+'px', 'width': brandItemWidth + 'px'});
    jQuery( window ).resize(function() {
        var resizeItemWidth = jQuery('#brand-list .item').width();
        jQuery('#brand-list .item > a').css({'height':resizeItemWidth+'px', 'width': resizeItemWidth + 'px'});
    });
    //As Soon on Page
    var asItemWidth = jQuery('.as-seen-on .item').width();
    jQuery('.as-seen-on .item > a').css({'height':asItemWidth+'px', 'width': asItemWidth + 'px'});
    jQuery( window ).resize(function() {
        var asResizeItemWidth = jQuery('.as-seen-on .item').width();
        jQuery('.as-seen-on .item > a').css({'height':asResizeItemWidth+'px', 'width': asResizeItemWidth + 'px'});
    });
});
/*
jQuery(window).load(function(){
    var categoryImageWidth = jQuery('#category-img-width img').width();
    var categoryImageheight= jQuery('#category-img-width img').height();
    var paddingVal = (categoryImageWidth - categoryImageheight -25)/2;
    jQuery('#category-img-width img').css({'padding-top': paddingVal+'px', 'padding-bottom':  paddingVal+'px'});
});*/
