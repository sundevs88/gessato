<?php
require_once 'app/Mage.php';
umask(0);
Mage::app('default');

$time = time();
$fromTime = $time - 64800;
$to = date('Y-m-d H:i:s', $fromTime);
$lastTime = $time - 151200; // 60*60*42
$from = date('Y-m-d H:i:s', $lastTime);
/**echo 'fromtime is '.$fromTime;
echo 'totime is '.$lastTime;
echo 'about to pull orders';**/
$order_items = Mage::getModel('sales/order')->getCollection()
    ->addAttributeToFilter('created_at', array('from' => $from, 'to' => $to));
/**echo 'orders pulled';**/
foreach ($order_items as $order) {
    $storeId = $order->getStore()->getId();
    if ($storeId == 1) {
        $state = $order->getStatus();
      /** echo "order status is ".$state."<br/>";**/   
        if($state != 'backorder' || $state != 'closed' || $state != 'marked_paid' || $state != 'complete'){
            if($state == 'pending_payment' || $state == 'processing') {
                try {
                    if(!$order->canInvoice()) {
                        Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
                    }
                    $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                    if (!$invoice->getTotalQty()) {
                        Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
                    }
                    $invoice->setRequestedCaptureCase(
Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE
);
                    $invoice->register();
                    $invoice->getOrder()->setCustomerNoteNotify(false); 
                    $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                    $transactionSave->save();
                    }
                catch (Mage_Core_Exception $e) {
                }
                    $state = $order->getStatus();
                    echo $state;
                    if ($state == 'processing' || $state == 'pending_payment' || $state == 'pending_paypal') {
                        $state = 'processing';
                        $status = 'marked_paid';
                        $comment = 'Automatic Invoice/Status Script - State changed to PAID';
                        $isCustomerNotified = false;
                        $order->setState($state,$status,$comment,$isCustomerNotified);
                        $order->save();
                    }       
            }
        }
    }
}


?>

