<?php
/**
* Copyright © Pulsestorm LLC: All rights reserved
*/
interface Stephanetravostino_Commercebug_Model_Interface_Ison
{
    public function isOn();
}