<?php

class Apptha_Outofstocknotification_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	$this->loadLayout();     
		$this->renderLayout();
		
    }
    public function storeNotificationProductDataAction(){
    	
    	$notifySuccessMes =  Mage::getStoreConfig('Outofstocknotification/general/activate_apptha_outofstock_notify_success_mes');
    	$statusOfInsert = Mage::getModel('outofstocknotification/outofstocknotification')->notifyDataInserted();
    	$statusOfInsert = intval($statusOfInsert);
    	if($statusOfInsert)
    	{
    		echo $notifySuccessMes;
    	}
    	else{
    		echo "okay"; //if product is already notified then we ask to another id
    	}
    }
}