<?php
class Apptha_Outofstocknotification_Block_Adminhtml_Outofstocknotification extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_outofstocknotification';
    $this->_blockGroup = 'outofstocknotification';
    $this->_headerText = Mage::helper('outofstocknotification')->__('Notified Customers Grid View');
    parent::__construct();
    $this->_removeButton('add');
  }
}