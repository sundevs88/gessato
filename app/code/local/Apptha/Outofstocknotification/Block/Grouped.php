<?php

class Apptha_Outofstocknotification_Block_Grouped extends Mage_Catalog_Block_Product_View_Type_Grouped
{                                                         
    protected function _prepareLayout()
    {
        $simpleBlock  = $this->getLayout()->getBlock('product.info.simple');
        $virtualBlock = $this->getLayout()->getBlock('product.info.virtual');
        
        $groupedBlock = $this->getLayout()->getBlock('product.info.grouped');
        
        $configurableBlock = $this->getLayout()->getBlock('product.info.configurable');
        
        
        if ($simpleBlock) {
            $simpleBlock->setTemplate('outofstocknotification/view.phtml');
        }
        else if ($virtualBlock) {
            $virtualBlock->setTemplate('outofstocknotification/view.phtml');
        }
        else if($configurableBlock){
            $configurableBlock->setTemplate('outofstocknotification/view.phtml');
        }
        else if($groupedBlock){
            $groupedBlock->setTemplate('outofstocknotification/grouped.phtml');
        }

    }
}

?>
