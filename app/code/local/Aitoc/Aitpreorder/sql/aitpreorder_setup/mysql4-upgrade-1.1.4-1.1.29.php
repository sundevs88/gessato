<?php
/**
 * Pre-Orders
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitpreorder
 * @version      1.2.11
 * @license:     5soxLTftyQhxyplGaD4S3STlnowRzK2YqbvycBpgEF
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2012 AITOC, Inc.
 */

$this->startSetup();

$preorderscriptId = $this->getAttribute('catalog_product', 'preorderdescript', 'attribute_id');

if ($preorderscriptId)
{
	$this->run(
        ' UPDATE ' . $this->getTable('catalog/eav_attribute') .
		' SET is_global = ' . Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE .
		' WHERE	attribute_id = ' . $preorderscriptId
    );
}

$this->endSetup();

?>