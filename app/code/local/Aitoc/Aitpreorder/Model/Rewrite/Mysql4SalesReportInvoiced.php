<?php
/**
 * Pre-Orders
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitpreorder
 * @version      1.2.11
 * @license:     5soxLTftyQhxyplGaD4S3STlnowRzK2YqbvycBpgEF
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2009 AITOC, Inc. 
 */
class Aitoc_Aitpreorder_Model_Rewrite_Mysql4SalesReportInvoiced extends Mage_Sales_Model_Mysql4_Report_Invoiced
{
    protected function _aggregateByInvoiceCreatedAt($from, $to)
    {
        $model = Mage::helper('aitpreorder')->retrieveAppropriateVersionClass('mysql4_report_invoiced');
        return $model->_aggregateByInvoiceCreatedAt($from, $to);
    }

    protected function _aggregateByOrderCreatedAt($from, $to)
    {
        $model = Mage::helper('aitpreorder')->retrieveAppropriateVersionClass('mysql4_report_invoiced');
        return $model->_aggregateByOrderCreatedAt($from, $to);
    }
}