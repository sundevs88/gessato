<?php
/**
 * Pre-Orders
 *
 * @category:    Aitoc
 * @package:     Aitoc_Aitpreorder
 * @version      1.2.11
 * @license:     5soxLTftyQhxyplGaD4S3STlnowRzK2YqbvycBpgEF
 * @copyright:   Copyright (c) 2015 AITOC, Inc. (http://www.aitoc.com)
 */
/**
 * @copyright  Copyright (c) 2012 AITOC, Inc. 
 */
class Aitoc_Aitpreorder_Model_Email extends Mage_ProductAlert_Model_Email
{ 
    protected function _getStockBlock()
    {
        if (is_null($this->_stockBlock)) {
            $this->_stockBlock = Mage::helper('productalert')
                ->createBlock('aitpreorder/email_stock');
        }
		return $this->_stockBlock;
    }
}