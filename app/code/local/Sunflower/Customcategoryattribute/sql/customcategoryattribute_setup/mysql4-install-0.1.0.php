<?php
$installer = $this;
$installer->startSetup();
$attribute  = array(
	'type'          =>  'text',
	'label'         =>  'Category Excerpt',
	'input'         =>  'textarea',
	'global'        =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible'       =>  true,
	'required'      =>  false,
	'user_defined'  =>  true,
	'default'       =>  "",
	'group'         =>  "General Information"
);
$installer->addAttribute('catalog_category', 'category_excerpt', $attribute);

$attribute  = array(
	'type'          =>  'int',
	'label'         =>  'Is Sub',
	'input'         =>  'select',
	'source'        => 'eav/entity_attribute_source_boolean',
	'global'        =>  Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible'       =>  true,
	'required'      =>  false,
	'user_defined'  =>  true,
	'default'       =>  0,
	'group'         =>  "General Information"
);
$installer->addAttribute('catalog_category', 'is_custom_sub', $attribute);

$installer->endSetup();
?>