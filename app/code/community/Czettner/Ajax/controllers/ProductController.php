<?php

require_once 'Mage/Catalog/controllers/ProductController.php';

class Czettner_Ajax_ProductController extends Mage_Catalog_ProductController {

    protected $_product;

    protected function _initProductLayout($product)
    {
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();

        $update->addHandle('PRODUCT_TYPE_'.$product->getTypeId());
        $update->addHandle('PRODUCT_'.$product->getId());

        if ($product->getPageLayout()) {
            $this->getLayout()->helper('page/layout')
                ->applyHandle($product->getPageLayout());
        }

        $this->loadLayoutUpdates();


        $update->addUpdate($product->getCustomLayoutUpdate());

        $this->generateLayoutXml()->generateLayoutBlocks();

        if ($product->getPageLayout()) {
            $this->getLayout()->helper('page/layout')
                ->applyTemplate($product->getPageLayout());
        }

        $currentCategory = Mage::registry('current_category');

        return $this;
    }


    public function quickViewAction() {
        /*
           if (!$this->getRequest()->isXmlHttpRequest()) {
           $this->_redirect('/');
           }
         */
        if ($product = $this->_initProduct()) {
            Mage::dispatchEvent('catalog_controller_product_view', array('product'=>$product));

            if ($this->getRequest()->getParam('options')) {
                $notice = $product->getTypeInstance(true)->getSpecifyOptionMessage();
                Mage::getSingleton('catalog/session')->addNotice($notice);
            }

            Mage::getModel('catalog/design')->applyDesign($product, Mage_Catalog_Model_Design::APPLY_FOR_PRODUCT);

            $this->_initProductLayout($product);
            $this->_product = $product->getId();

            $this->getResponse()->setBody( $this->_getContentHtml() );

        } else {
            echo Mage::helper('catalog')->__('Product not found');
        }
    }

    protected function _getContentHtml()
    {
        /*$layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('ajax_product_view');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;*/

		$html = $this->getLayout()
        ->createBlock('catalog/product_view','product-quickview-data')
        ->setTemplate('ajax/quickview.phtml');

		$productpriceBlock = $this->getLayout()->createBlock('catalog/product_view_type_simple', 'product_type_data', array('template'=>'catalog/product/view/type/default.phtml'));

		$productoptionsBlock = $this->getLayout()->createBlock('catalog/product_view_options', 'product_options')
							->addOptionRenderer('text','catalog/product_view_options_type_text','catalog/product/view/options/type/text.phtml')
							->addOptionRenderer('file','catalog/product_view_options_type_file','catalog/product/view/options/type/file.phtml')
							->addOptionRenderer('select','catalog/product_view_options_type_select','catalog/product/view/options/type/select.phtml')
							->addOptionRenderer('date','catalog/product_view_options_type_date','catalog/product/view/options/type/date.phtml');

		$productoptionsBlock->setTemplate('catalog/product/view/options.phtml');

		$this->getLayout()->getBlock('product-quickview-data')
            ->append($productpriceBlock)
			->append($productoptionsBlock);

		return $html->toHtml();
    }

}

