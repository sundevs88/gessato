<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_OembedController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {

        if ($this->_getConfigHelper()->getSelectedMethod() != 'oembed') {
            $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found', true);
            $this->getResponse()->setHeader('Status', '404 File not found', true);

            $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultNoRoute');
            }
        } else {
            if (Mage::app()->getRequest()->has('url')) {
                $url = Mage::app()->getRequest()->getParam('url');

                $product = $this->_getProductFromUrl($this->_prepareUrl($url));

                if ($product->getId()) {
                    $json = Mage::getModel('richpins/product_type_factory')->createProductType($product)->getJson();
                    $this->getResponse()->setHeader('Content-Type', 'application/json');
                    $this->getResponse()->setBody($json);
                } else {
                    $this->getResponse()->setBody('oEmbed URL not recognized type');
                    $this->getResponse()->setHttpResponseCode(404);
                }
            } else {
                $this->getResponse()->setBody("Expected param 'url'.");
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
    }

    protected function _getProductFromUrl($url) {
        $rewrite = Mage::getModel('core/url_rewrite')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByRequestPath($url);
        return Mage::getModel('catalog/product')->load($rewrite->getProductId());
    }

    protected function _prepareUrl($url) {
        $search = array(
            Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
            'index.php/',
        );
        return str_replace($search, '', $url);
    }

    /**
     *
     * @return Creatuity_RichPins_Helper_Config
     */
    protected function _getConfigHelper() {
        return Mage::helper('richpins/config');
    }

}
