################################################################################
#                                                                              #
#   Connection Bridge                                                          #
#                                                                              #
#   Cart2Cart - Shopping Cart Migration Service                                #
#   (c) MagneticOne.com                                                        #
#                                                                              #
################################################################################

Dear Customer,

Thank you for choosing Cart2Cart. This manual will help you install this Connection Bridge
on your site in a few minutes. If you experience any problems during installation,
don't hesitate to contact us at: https://support.magneticone.com/index.php?/Tickets/Submit
We will be happy to help you.

How to install target bridge:

1. Upload "bridge2cart" folder from this archive to your store root directory

2. Set the permissions as following:

  "bridge.php" - 644 or 666, depending on your server configuration;
  "/bridge2cart" folder - 755 or 777, depending on your server configuration.

3. To be able to copy images during your migration please check the writing permissions
on your Target store for image directory and ALL subfolders/files it contains (chmod 777).

To check if the Bridge files are working correctly,
enter address line: http://[yourstore url]/bridge2cart/bridge.php in your browser.
If you see “ERROR_BRIDGE_VERSION_NOT_SUPPORTED” the bridge is working CORRECTLY.
If not, please update permissions for items mentioned above once again.

NOTE!
We strongly recommend to turn off redirect on your site for proper work of the Connection Bridge files.
To check if you need to turn off redirect, follow this link "http://[yourstore url]/bridge2cart/bridge.php"
and once it's redirecting to some other link you should disable redirects in your .htaccess file.
It's a hidden file residing in your "shop" folder.

If you have any questions, check out the following link:
http://www.shopping-cart-migration.com/faq/1-general-questions/47-what-are-the-connection-bridge-files-and-how-to-download-them


Yours,
Cart2Cart Team
http://www.shopping-cart-migration.com/